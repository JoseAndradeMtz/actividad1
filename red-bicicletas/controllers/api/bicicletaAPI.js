var Bicicleta = require('../../models/bicicleta');
exports.getBicicletas = function(req,res)
{
 res.status(200).json({bicicletas: Bicicleta.allBicis});
}
exports.delBicicleta = function(req,res)
{
 Bicicleta.delBicicletaId(req.body.id);
 res.status(204).send();
}
exports.setBicicleta = function(req,res)
{
 var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo);
 bici.ubicacion = [req.body.lat,req.body.lng];
 Bicicleta.add(bici);
 res.status(200).json({bicicleta: bici});
}
