var Bicicleta = function(id, color, modelo, ubicacion)
{
 this.id = id;
 this.color = color;
 this.modelo = modelo;
 this.ubicacion = ubicacion;
}

 Bicicleta.prototype.toString = function()
 {
  return 'id: '+this.id+' | color: '+this.color;
 }

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici)
{
 Bicicleta.allBicis.push(aBici);
}
Bicicleta.getBicicletaId = function(biciId)
{
 var bici = Bicicleta.allBicis.find(x => x.id == biciId);
 if(bici)
   return bici;
 else
 	 throw new Error('No existe una bicicleta con el id '+biciId);
}
Bicicleta.delBicicletaId = function(biciId)
{
 for(var i = 0; i < Bicicleta.allBicis.length; i++)
 {
  if(Bicicleta.allBicis[i].id == biciId)
  {
   Bicicleta.allBicis.splice(i,1);
   break;
  }
 } 
}
var a = new Bicicleta(1,'rojo','urbana',[22.0121166,-99.0574275]);
var b = new Bicicleta(2,'blanca','urbana',[22.0123856,-99.0573388]);
var c = new Bicicleta(3,'verde','urbana',[22.009261, -99.054776]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);

module.exports = Bicicleta;
