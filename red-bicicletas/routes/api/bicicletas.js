var express =require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaAPI');

router.get('/', bicicletaController.getBicicletas);
router.post('/setBicicleta', bicicletaController.setBicicleta);
router.post('/delBicicleta', bicicletaController.delBicicleta);

module.exports = router;
