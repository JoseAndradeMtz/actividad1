    var mymap = L.map('mapid').setView([22.009261, -99.054776], 13);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11',
    maxZoom: 32,
    accessToken: 'pk.eyJ1IjoicHIzZDR0MHIzIiwiYSI6ImNrZ2lmZng4aDFteHIyeGxqNTJvMmtpZGYifQ.j4I5dRx77yhtAds_u_ZuRA'
}).addTo(mymap);
/*L.marker([22.009261, -99.054776]).addTo(mymap)
    .bindPopup('Aquí vivo yo :D')
    .openPopup();
L.marker([22.0123856,-99.0573388]).addTo(mymap)
    .bindPopup('Aquí vive mi mama :D')
    .openPopup();
L.marker([22.0121166,-99.0574275]).addTo(mymap)
    .bindPopup('Aquí vive mi hermana :D')
    .openPopup();*/
  $.ajax(
 {
    type: "GET",
    dataType:"JSON",
    url: "http://184.173.57.237:3000/api/bicicletas",
    beforeSend: function(event)
    {
    },
    success: function(server)
    {
      if (server.bicicletas.length > 0)
      {
        $.each(server.bicicletas,function (a,b)
        {
console.log(b.modelo)
	  L.marker(b.ubicacion).addTo(mymap)
          .bindPopup(b.modelo+' '+b.color)
          .openPopup();
        });
      }
      else
	alert("No hay bicicletas");
    },
    error: function(e)
    {
     console.log(e);
    }
  });
